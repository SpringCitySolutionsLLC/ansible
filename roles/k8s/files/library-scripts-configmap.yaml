# roles/k8s/files/library-scripts-configmap.yaml
#
# Mostly for use with linuxserver.io webtop and code-server
# symlink this to each role
# For example, symlink to roles/python-dev/tasks/library-scripts-configmap.yaml
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: library-scripts-configmap
data:
  git.sh: |
    #!/bin/bash
    # /config/scripts/library/git.sh
    #
    case $1 in
      initial)
        git config --global user.name "Vince Mulhollon ($2)"
        git config --global user.email "vince.mulhollon@springcitysolutions.com"
        git config --global init.defaultBranch master
        ;;
      version)
        echo git
        git --version
        echo
        ;;
    esac
    #
    exit 0
  ssh.sh: |
    #!/bin/bash
    # /config/scripts/library/ssh.sh
    #
    # Has to run after glab.sh and after gh.sh
    #
    case $1 in
      initial)
        # SSH key for Gitlab and Github
        # The linuxserver.io images have two root users
        # The regular root is at /root and the web login is user abc at /config both user 0
        mkdir -p /config/.ssh
        if [ ! -f /config/.ssh/id_rsa ]; then
          ssh-keygen -t rsa -C "$2" -N "" -f /config/.ssh/id_rsa
        fi
        mkdir -p /root/.ssh
        if [ ! -f /root/.ssh/id_rsa ]; then
          cp /config/.ssh/id_rsa* /root/.ssh/
        fi
        #
        eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
        echo
        if [ -f /config/gitlabtoken.txt ]; then
          glab ssh-key add /config/.ssh/id_rsa.pub -t "$2"
        fi
        echo
        if [ -f /config/githubtoken.txt ]; then
          gh ssh-key add /config/.ssh/id_rsa.pub --title "$2"
        fi
        echo
        ;;
    esac
    #
    exit 0
  kde.sh: |
    #!/bin/bash
    # /config/scripts/library/kde.sh
    #
    case $1 in
      initial)
        # KWIN
        /usr/bin/plasma-apply-desktoptheme breeze-light
        /usr/bin/plasma-apply-lookandfeel --resetLayout -a org.kde.breeze.desktop
        /usr/bin/plasma-apply-colorscheme BreezeLight
        mkdir -p ~/.background
        if [ ! -f /config/.background/bg.jpg ]; then
          wget -O /config/.background/bg.jpg https://solidbackgrounds.com/images/1920x1080/1920x1080-columbia-blue-solid-color-background.jpg
        fi
        /usr/bin/plasma-apply-wallpaperimage /config/.background/bg.jpg
        #
        # KONSOLE
        mkdir -p ~/.local/share/konsole
        echo "[Appearance]" > ~/.local/share/konsole/custom.profile
        echo "ColorScheme=BlackOnRandomLight" >> ~/.local/share/konsole/custom.profile
        echo "Font=Hack,17,-1,7,50,0,0,0,0,0" >> ~/.local/share/konsole/custom.profile
        echo >> ~/.local/share/konsole/custom.profile
        echo "[Cursor Options]" >> ~/.local/share/konsole/custom.profile
        echo "CustomCursorColor=0,255,0" >> ~/.local/share/konsole/custom.profile
        echo "UseCustomCursorColor=true" >> ~/.local/share/konsole/custom.profile
        echo >> ~/.local/share/konsole/custom.profile
        echo "[General]" >> ~/.local/share/konsole/custom.profile
        echo "Command=/bin/bash" >> ~/.local/share/konsole/custom.profile
        echo "Name=custom" >> ~/.local/share/konsole/custom.profile
        echo "Parent=FALLBACK/" >> ~/.local/share/konsole/custom.profile
        echo >> ~/.local/share/konsole/custom.profile
        echo "[Interaction Options]" >> ~/.local/share/konsole/custom.profile
        echo "UnderlineLinksEnabled=false" >> ~/.local/share/konsole/custom.profile
        echo >> ~/.local/share/konsole/custom.profile
        echo "[Scrolling]" >> ~/.local/share/konsole/custom.profile
        echo "ScrollBarPosition=1" >> ~/.local/share/konsole/custom.profile
        kwriteconfig5 --file ~/.config/konsolerc --group 'Desktop Entry' --key DefaultProfile custom.profile
        kwriteconfig5 --file ~/.config/konsolerc --group TabBar --key NewTabButton true
        kwriteconfig5 --file ~/.config/konsolerc --group TabBar --key TabBarVisibility AlwaysShowTabBar
        ;;
    esac
    #
    exit 0
  bash-completion.sh: |
    #!/bin/bash
    # /config/scripts/library/bash-completion.sh
    #
    case $1 in
      initial)
        # Your deployment needs to include the bash-completion package
        LINE='source /etc/bash_completion'
        FILE='/config/.bashrc'
        grep -qF -- "$LINE" "$FILE" || echo "$LINE" >> "$FILE"
        ;;
    esac
    #
    exit 0
  brew.sh: |
    #!/bin/bash
    # /config/scripts/library/brew.sh
    #
    case $1 in
      initial)
        NONINTERACTIVE=1 /usr/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
        LINE='eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"'
        FILE='/config/.bashrc'
        grep -qF -- "$LINE" "$FILE" || echo "$LINE" >> "$FILE"
        ;;
      update)
        eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
        #
        echo brew update
        brew update
        echo
        #
        echo brew upgrade
        brew upgrade
        echo
        #
        echo brew cleanup
        brew cleanup
        echo
        ;;
      version)
        eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
        #
        echo brew
        brew --version
        echo
        ;;
    esac
    #
    exit 0
  glab.sh: |
    #!/bin/bash
    # /config/scripts/library/glab.sh
    #
    # Has to run after brew.sh
    #
    # gitlab-cli aka glab
    # https://docs.gitlab.com/ee/editor_extensions/gitlab_cli/
    # https://gitlab.com/gitlab-org/cli
    #
    case $1 in
      initial)
        eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
        brew install glab
        glab completion -s bash > /etc/bash_completion.d/glab
        # Below can be removed by October - it should have been run on all systems by then
        if [ -f /config/myaccesstoken.txt ]; then
          mv /config/myaccesstoken.txt /config/gitlabtoken.txt
        fi
        # Above can be removed by October
        if [ -f /config/gitlabtoken.txt ]; then
          glab auth login --stdin < /config/gitlabtoken.txt
        fi
        ;;
      version)
        eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
        echo glab
        glab --version
        echo
        ;;
    esac
    #
    exit 0
  golang.sh: |
    #!/bin/bash
    # /config/scripts/library/golang.sh
    #
    # As seen in a web browser at https://go.dev/dl/
    GOFILE='go1.22.5.linux-amd64.tar.gz'
    #
    case $1 in
    initial)
      if [ ! -f /config/$GOFILE ]; then
        wget -O /config/$GOFILE https://go.dev/dl/$GOFILE
      fi
      rm -rf /usr/local/go
      tar -C /usr/local -xzf /config/$GOFILE
      LINE='export PATH=$PATH:/usr/local/go/bin'
      FILE='/config/.bashrc'
      grep -qF -- "$LINE" "$FILE" || echo "$LINE" >> "$FILE"
      echo Probably have to restart terminal because of changes made to .bashrc
      ;;
    version)
      echo go
      /usr/local/go/bin/go version
      echo
      ;;
    esac
    #
    exit 0
  dotnet.sh: |
    #!/bin/bash
    # /config/scripts/library/dotnet.sh
    #
    case $1 in
    initial)
      echo 'debconf debconf/frontend select Readline' | debconf-set-selections
      apt install -y software-properties-common
      add-apt-repository ppa:dotnet/backports -y
      apt install -y dotnet8
      ;;
    version)
      echo dotnet
      dotnet --version
      echo
      ;;
    esac
    #
    exit 0
  amber.sh: |
    #!/bin/bash
    # /config/scripts/library/amber.sh
    #
    case $1 in
      initial)
        curl -s "https://raw.githubusercontent.com/Ph0enixKM/AmberNative/master/setup/install.sh" | bash -s -- --user
        ;;
      version)
        echo amber
        amber --version
        echo
        ;;
    esac
    #
    exit 0
  sdkman.sh: |
    #!/bin/bash
    # /config/scripts/library/sdkman.sh
    #
    case $1 in
      initial)
        curl -s 'https://get.sdkman.io' | /usr/bin/bash
        ;;
      update)
        source "$HOME/.sdkman/bin/sdkman-init.sh"
        #
        echo sdkman update
        sdk update
        echo
        #
        echo sdk selfupdate
        sdk selfupdate
        echo
        #
        echo sdk upgrade
        sdk upgrade
        echo
        #
        echo sdk flush
        sdk flush
        echo
        ;;
      version)
        source "$HOME/.sdkman/bin/sdkman-init.sh"
        #
        echo sdk version
        sdk version
        echo
        #
        echo sdk current
        sdk current
        echo
        ;;
    esac
    #
    exit 0
  java.sh: |
    #!/bin/bash
    # /config/scripts/library/java.sh
    #
    # Has to run after sdkman.sh
    #
    # As seen in webtop, "sdk list java | grep open", last column in the Java.net section
    JAVAVERSION='23.ea.29-open'
    #
    case $1 in
      initial)
        source "$HOME/.sdkman/bin/sdkman-init.sh"
        #
        sdk install java
        sdk install java $JAVAVERSION
        sdk env java $JAVAVERSION
        sdk default java $JAVAVERSION
        #
        sdk install maven
        ;;
      update)
        source "$HOME/.sdkman/bin/sdkman-init.sh"
        #
        echo sdkman update
        sdk update
        echo
        #
        echo sdk upgrade
        sdk upgrade
        echo
        #
        echo sdk flush
        sdk flush
        echo
        ;;
      version)
        source "$HOME/.sdkman/bin/sdkman-init.sh"
        #
        echo java version
        java --version
        echo
        #
        echo mvn version
        mvn --version
        echo
        ;;
    esac
    #
    exit 0
  clojure.sh: |
    #!/bin/bash
    # /config/scripts/library/clojure.sh
    #
    # Has to run after brew.sh
    #
    # https://clojure.org/guides/install_clojure
    #
    case $1 in
      initial)
        eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
        brew install clojure/tools/clojure
        ;;
      version)
        eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
        echo clj
        clj --version
        echo
        ;;
    esac
    #
    exit 0
  leiningen.sh: |
    #!/bin/bash
    # /config/scripts/library/leiningen.sh
    #
    # Has to run after brew.sh
    #
    # https://formulae.brew.sh/formula/leiningen
    #
    case $1 in
      initial)
        eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
        brew install leiningen
        ;;
      version)
        eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
        echo leiningen
        lein --version
        echo
        ;;
    esac
    #
    exit 0
  maven.sh: |
    #!/bin/bash
    # /config/scripts/library/maven.sh
    #
    # Has to run after sdkman.sh
    #
    case $1 in
      initial)
        source "$HOME/.sdkman/bin/sdkman-init.sh"
        #
        sdk install maven
        ;;
      version)
        source "$HOME/.sdkman/bin/sdkman-init.sh"
        #
        echo mvn version
        mvn --version
        echo
        ;;
    esac
    #
    exit 0
  haskell.sh: |
    #!/bin/bash
    # /config/scripts/library/haskell.sh
    #
    case $1 in
      initial)
        # GHCup
        # https://www.haskell.org/ghcup/#
        LINE='source /config/.ghcup/env'
        FILE='/config/.bashrc'
        grep -qF -- "$LINE" "$FILE" || echo "$LINE" >> "$FILE"
        echo Probably have to restart shell because of changes made to .bashrc
        #
        # As documented at https://github.com/haskell/ghcup-hs/blob/master/scripts/bootstrap/bootstrap-haskell
        export BOOTSTRAP_HASKELL_NONINTERACTIVE=1
        export BOOTSTRAP_HASKELL_INSTALL_HLS=1
        curl --proto '=https' --tlsv1.2 -sSf https://get-ghcup.haskell.org | sh
        ;;
      update)
        source "$HOME/.ghcup/env"
        #
        ghcup upgrade
        echo
        echo You probably want to run ghcup tui for the upgrade interface
        echo
        ;;
      version)
        source "$HOME/.ghcup/env"
        #
        echo ghci version
        ghci --version
        echo
        ;;
    esac
    #
    exit 0
  nvm.sh: |
    #!/bin/bash
    # /config/scripts/library/nvm.sh
    #
    case $1 in
      initial)
        #
        # https://github.com/nvm-sh/nvm?tab=readme-ov-file#git-install
        #
        rm -Rf /config/.nvm
        git clone https://github.com/nvm-sh/nvm.git /config/.nvm
        #
        cd /config/.nvm
        git fetch --tags origin
        git checkout `git describe --abbrev=0 --tags --match "v[0-9]*" $(git rev-list --tags --max-count=1)`
        #
        LINE='export NVM_DIR="$HOME/.nvm"'
        FILE='/config/.bashrc'
        grep -qF -- "$LINE" "$FILE" || echo "$LINE" >> "$FILE"
        #
        LINE='[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"'
        FILE='/config/.bashrc'
        grep -qF -- "$LINE" "$FILE" || echo "$LINE" >> "$FILE"
        #
        LINE='[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"'
        FILE='/config/.bashrc'
        grep -qF -- "$LINE" "$FILE" || echo "$LINE" >> "$FILE"
        #
        echo Probably have to restart shell because of changes made to .bashrc
        #
        source /config/.nvm/nvm.sh
        #
        ;;
      update)
        cd /config/.nvm
        git fetch --tags origin
        git checkout `git describe --abbrev=0 --tags --match "v[0-9]*" $(git rev-list --tags --max-count=1)`
        ;;
      version)
        source /config/.nvm/nvm.sh
        #
        echo nvm version
        nvm --version
        echo
        ;;
    esac
    #
    exit 0
  nodejs.sh: |
    #!/bin/bash
    # /config/scripts/library/nodejs.sh
    #
    case $1 in
      initial)
        source /config/.nvm/nvm.sh
        #
        nvm install --lts
        nvm use --lts
        ;;
      update)
        source /config/.nvm/nvm.sh
        #
        nvm install --lts
        nvm use --lts
        ;;
      version)
        source /config/.nvm/nvm.sh
        #
        echo npm version
        npm -v
        echo
        #
        # node
        echo node version
        node -v
        echo
        ;;
    esac
    #
    exit 0
  solidity.sh: |
    #!/bin/bash
    # /config/scripts/library/solidity.sh
    #
    case $1 in
      initial)
        echo 'debconf debconf/frontend select Readline' | debconf-set-selections
        apt install -y software-properties-common
        add-apt-repository ppa:ethereum/ethereum -y
        apt install solc
        ;;
      update)
        apt install solc
        ;;
      version)
        echo solc version
        solc --version
        echo
        ;;
    esac
    #
    exit 0
  gh.sh: |
    #!/bin/bash
    # /config/scripts/library/gh.sh
    #
    # Has to run after brew.sh
    #
    # github cli aka gh
    # https://cli.github.com/
    # https://cli.github.com/manual/
    #
    case $1 in
      initial)
        eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
        brew install gh
        if [ -f /config/githubtoken.txt ]; then
          gh auth login --with-token < /config/githubtoken.txt
        fi
        ;;
      version)
        eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
        echo gh
        gh --version
        echo
        ;;
    esac
    #
    exit 0
  cpmtools.sh: |
    #!/bin/bash
    # /config/scripts/library/cpmtools.sh
    #
    # Has to run after brew.sh
    #
    # cpmtools for disk access
    # https://formulae.brew.sh/formula/cpmtools
    # http://www.moria.de/~michael/cpmtools/
    #
    case $1 in
      initial)
        eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
        brew install cpmtools
        ;;
    esac
    #
    exit 0
  mtools.sh: |
    #!/bin/bash
    # /config/scripts/library/mtools.sh
    #
    # Has to run after brew.sh
    #
    # mtools for disk access
    # https://formulae.brew.sh/formula/mtools
    # https://www.gnu.org/software/mtools/
    #
    case $1 in
      initial)
        eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
        brew install mtools
        ;;
    esac
    #
    exit 0
  dosbox-x.sh: |
    #!/bin/bash
    # /config/scripts/library/dosbox-x.sh
    #
    # https://dosbox-x.com/
    # https://dosbox-x.com/wiki/Home
    #
    case $1 in
      initial)
        echo 'debconf debconf/frontend select Readline' | debconf-set-selections
        apt install -y dosbox-x dosbox-x-data
        ;;
    esac
    #
    exit 0
  qemu.sh: |
    #!/bin/bash
    # /config/scripts/library/qemu.sh
    #
    # https://gunkies.org/wiki/Category:Qemu
    #
    case $1 in
      initial)
        echo 'debconf debconf/frontend select Readline' | debconf-set-selections
        apt install -y qemu-system
        ;;
    esac
    #
    exit 0
  erlang.sh: |
    #!/bin/bash
    # /config/scripts/library/erlang.sh
    #
    # Has to run after brew.sh
    #
    # https://formulae.brew.sh/formula/erlang
    # https://www.erlang.org/docs
    #
    case $1 in
      initial)
        eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
        brew install erlang
        ;;
    esac
    #
    exit 0
  elixir.sh: |
    #!/bin/bash
    # /config/scripts/library/elixir.sh
    #
    # Has to run after brew.sh
    #
    # https://formulae.brew.sh/formula/elixir
    # https://elixir-lang.org/install.html
    #
    case $1 in
      initial)
        eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
        brew install elixir
        ;;
    esac
    #
    exit 0
  swiprolog.sh: |
    #!/bin/bash
    # /config/scripts/library/swiprolog.sh
    #
    # Has to run after brew.sh
    #
    # https://formulae.brew.sh/formula/swi-prolog
    #
    case $1 in
      initial)
        eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
        brew install swi-prolog
        ;;
    esac
    #
    exit 0
  lua.sh: |
    #!/bin/bash
    # /config/scripts/library/lua.sh
    #
    # Has to run after brew.sh
    #
    # https://formulae.brew.sh/formula/lua
    # https://www.lua.org/
    #
    # https://formulae.brew.sh/formula/luarocks
    # https://luarocks.org/
    #
    case $1 in
      initial)
        eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
        brew install lua luarocks
        ;;
    esac
    #
    exit 0
...
