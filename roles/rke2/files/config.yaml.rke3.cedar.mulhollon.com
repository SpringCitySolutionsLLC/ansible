# /etc/rancher/rke2/config.yaml
#
# Installed by Ansible using roles/rke2/files/config.yaml.rke3.cedar.mulhollon.com
#
server: https://rke.cedar.mulhollon.com:9345
token: rke
#
tls-san:
  - rke.cedar.mulhollon.com
  - rke1.cedar.mulhollon.com
  - 10.10.8.241
  - rke2.cedar.mulhollon.com
  - 10.10.8.242
  - rke3.cedar.mulhollon.com
  - 10.10.8.243
  - rke4.cedar.mulhollon.com
  - 10.10.8.244
  - rke5.cedar.mulhollon.com
  - 10.10.8.245
  - rke6.cedar.mulhollon.com
  - 10.10.8.246
#
kube-proxy-arg:
  - proxy-mode=ipvs
  - ipvs-strict-arp=true
#
