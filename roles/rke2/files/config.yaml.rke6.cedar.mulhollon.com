# /etc/rancher/rke2/config.yaml
#
# Installed by Ansible using roles/rke2/files/config.yaml.rke6.cedar.mulhollon.com
#
server: https://rke.cedar.mulhollon.com:9345
token: rke
#
kube-proxy-arg:
  - proxy-mode=ipvs
  - ipvs-strict-arp=true
#
