#!/usr/bin/env bash

openssl s_client \
  -connect es01.cedar.mulhollon.com:9200 \
  -servername es01.cedar.mulhollon.com \
  -showcerts < /dev/null 2>/dev/null | \
  openssl x509 -in /dev/stdin -sha256 -noout -fingerprint | \
  sed 's/://g'  

