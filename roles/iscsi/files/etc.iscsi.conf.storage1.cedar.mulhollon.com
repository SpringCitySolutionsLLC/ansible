# /etc/iscsi.conf
#
# Configured by Ansible using file etc.iscsi.conf.storage1.cedar.mulhollon.com
#

t0 {
	TargetAddress   = 10.10.20.4
	TargetName      = iqn.2002-03.com.mulhollon:storage1
	AuthMethod      = None
}

#
