# roles/netbox/tasks/proxmox002.yml
---

- name: Proxmox002 on netbox
  when: (my_hostname == "proxmox002")
  delegate_to: 127.0.0.1
  tags: netbox
  block:

    - name: Netbox Proxmox Device
      netbox.netbox.netbox_device:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          site: "Utility Room"
          location: "Data Center Rack"
          device_type: "Microserver SYS-E200-8D"
          name: "{{ my_hostname }}"
          serial: "C101FAG23050219"
          cluster: "Proxmox"
          rack: "Datacenter Rack"
          face: "Front"
          description: "Proxmox VE node."
          platform: "Proxmox VE"
          device_role: "Infrastructure"
          status: "Active"

# Define LAGs or bonds first

    - name: Netbox bond0 LAG Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "bond0"
          description: "Dual 1GE"
          type: "Link Aggregation Group (LAG)"
          mtu: "9000"
          mode: "Tagged (All)"

    - name: Netbox bond1 LAG Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "bond1"
          description: "Dual 10GE"
          type: "Link Aggregation Group (LAG)"
          mtu: "9000"
          mode: "Tagged (All)"

# Physical ethernet interfaces connected to LAGs or bonds

    - name: Netbox eno1 Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "eno1"
          description: "eno1"
          lag: "bond0"
          type: "1000BASE-T (1GE)"
          mtu: "9000"
          speed: "1000000"
          duplex: "full"

    - name: Netbox eno2 Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "eno2"
          description: "eno2"
          lag: "bond0"
          type: "1000BASE-T (1GE)"
          mtu: "9000"
          speed: "1000000"
          duplex: "full"

    - name: Netbox eno3 Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "eno3"
          description: "eno3"
          lag: "bond1"
          type: "10GBASE-T (10GE)"
          mtu: "9000"
          speed: "10000000"
          duplex: "full"

    - name: Netbox eno4 Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "eno4"
          description: "eno4"
          lag: "bond1"
          type: "10GBASE-T (10GE)"
          mtu: "9000"
          speed: "10000000"
          duplex: "full"

# VLAN 010 PROD Production Traffic

    - name: Netbox bond0.010 VLAN Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "bond0.010"
          description: "PROD VLAN 802.1(p) 4"
          type: "Virtual"
          parent_interface: "bond0"
          mtu: "9000"
          mode: "Access"
          untagged_vlan:
            name: "PROD"

    - name: Netbox bond1.010 VLAN Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "bond1.010"
          description: "PROD VLAN 802.1(p) 4"
          type: "Virtual"
          parent_interface: "bond1"
          mtu: "9000"
          mode: "Access"
          untagged_vlan:
            name: "PROD"

    - name: Netbox vmbr010 Bridge Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "vmbr010"
          description: "PROD Bridge"
          type: "Virtual"
          parent_interface: "bond1.010"
          mtu: "9000"

    - name: Netbox PROD IP Address
      netbox.netbox.netbox_ip_address:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          assigned_object:
            device: "{{ my_hostname }}"
            name: "vmbr010"
          address: "10.10.8.2/16"
          dns_name: "{{ my_hostname }}.cedar.mulhollon.com"

# VLAN 030 MIGRATION Proxmox VM Migration Traffic

    - name: Netbox bond0.030 VLAN Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "bond0.030"
          description: "MIGRATION VLAN 802.1(p) 2"
          type: "Virtual"
          parent_interface: "bond0"
          mtu: "9000"
          mode: "Access"
          untagged_vlan:
            name: "MIGRATION"

    - name: Netbox bond1.030 VLAN Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "bond1.030"
          description: "MIGRATION VLAN 802.1(p) 2"
          type: "Virtual"
          parent_interface: "bond1"
          mtu: "9000"
          mode: "Access"
          untagged_vlan:
            name: "MIGRATION"

    - name: Netbox vmbr030 Bridge Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "vmbr030"
          description: "MIGRATION Bridge"
          type: "Virtual"
          parent_interface: "bond0.030"
          mtu: "9000"

    - name: Netbox MIGRATION IP Address
      netbox.netbox.netbox_ip_address:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          assigned_object:
            device: "{{ my_hostname }}"
            name: "vmbr030"
          address: "10.30.8.2/16"
          dns_name: "migration.{{ my_hostname }}.cedar.mulhollon.com"

# VLAN 031 COROSYNC1 Proxmox Corosync Cluster 1

    - name: Netbox bond0.031 VLAN Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "bond0.031"
          description: "COROSYNC1 VLAN 802.1(p) 7"
          type: "Virtual"
          parent_interface: "bond0"
          mtu: "9000"
          mode: "Access"
          untagged_vlan:
            name: "COROSYNC1"

    - name: Netbox bond1.031 VLAN Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "bond1.031"
          description: "COROSYNC1 VLAN 802.1(p) 7"
          type: "Virtual"
          parent_interface: "bond1"
          mtu: "9000"
          mode: "Access"
          untagged_vlan:
            name: "COROSYNC1"

    - name: Netbox vmbr031 Bridge Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "vmbr031"
          description: "COROSYNC1 Bridge"
          type: "Virtual"
          parent_interface: "bond0.031"
          mtu: "9000"

    - name: Netbox COROSYNC1 IP Address
      netbox.netbox.netbox_ip_address:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          assigned_object:
            device: "{{ my_hostname }}"
            name: "vmbr031"
          address: "10.31.8.2/16"
          dns_name: "corosync1.{{ my_hostname }}.cedar.mulhollon.com"

# VLAN 032 COROSYNC2 Proxmox Corosync Cluster 2

    - name: Netbox bond0.032 VLAN Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "bond0.032"
          description: "COROSYNC2 VLAN 802.1(p) 7"
          type: "Virtual"
          parent_interface: "bond0"
          mtu: "9000"
          mode: "Access"
          untagged_vlan:
            name: "COROSYNC2"

    - name: Netbox bond1.032 VLAN Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "bond1.032"
          description: "COROSYNC2 VLAN 802.1(p) 7"
          type: "Virtual"
          parent_interface: "bond1"
          mtu: "9000"
          mode: "Access"
          untagged_vlan:
            name: "COROSYNC2"

    - name: Netbox vmbr032 Bridge Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "vmbr032"
          description: "COROSYNC2 Bridge"
          type: "Virtual"
          parent_interface: "bond1.032"
          mtu: "9000"

    - name: Netbox COROSYNC2 IP Address
      netbox.netbox.netbox_ip_address:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          assigned_object:
            device: "{{ my_hostname }}"
            name: "vmbr032"
          address: "10.32.8.2/16"
          dns_name: "corosync2.{{ my_hostname }}.cedar.mulhollon.com"

# VLAN 050 ISP Layer 2 between ISP and Firewall

    - name: Netbox bond0.050 VLAN Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "bond0.050"
          description: "ISP VLAN 802.1(p) 3"
          type: "Virtual"
          parent_interface: "bond0"
          mtu: "9000"
          mode: "Access"
          untagged_vlan:
            name: "ISP"

    - name: Netbox bond1.050 VLAN Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "bond1.050"
          description: "ISP VLAN 802.1(p) 3"
          type: "Virtual"
          parent_interface: "bond1"
          mtu: "9000"
          mode: "Access"
          untagged_vlan:
            name: "ISP"

    - name: Netbox vmbr050 Bridge Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "vmbr050"
          description: "ISP Bridge"
          type: "Virtual"
          parent_interface: "bond0.050"
          mtu: "9000"

# VLAN 060 SDN VLAN Zone

    - name: Netbox bond0.060 VLAN Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "bond0.060"
          description: "VLAN Zone VLAN 802.1(p) 5"
          type: "Virtual"
          parent_interface: "bond0"
          mtu: "9000"
          mode: "Access"
          untagged_vlan:
            name: "VLAN ZONE"

    - name: Netbox bond1.060 VLAN Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "bond1.060"
          description: "VLAN Zone VLAN 802.1(p) 5"
          type: "Virtual"
          parent_interface: "bond1"
          mtu: "9000"
          mode: "Access"
          untagged_vlan:
            name: "VLAN ZONE"

    - name: Netbox vmbr060 Bridge Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "vmbr060"
          description: "VLAN Zone Bridge"
          type: "Virtual"
          parent_interface: "bond0.060"
          mtu: "9000"

# VLAN 070 SDN QinQ Zone

    - name: Netbox bond0.070 VLAN Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "bond0.070"
          description: "QinQ Zone VLAN 802.1(p) 5"
          type: "Virtual"
          parent_interface: "bond0"
          mtu: "9000"
          mode: "Access"
          untagged_vlan:
            name: "QINQ ZONE"

    - name: Netbox bond1.070 VLAN Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "bond1.070"
          description: "QinQ Zone VLAN 802.1(p) 5"
          type: "Virtual"
          parent_interface: "bond1"
          mtu: "9000"
          mode: "Access"
          untagged_vlan:
            name: "QINQ ZONE"

    - name: Netbox vmbr070 Bridge Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "vmbr070"
          description: "QinQ Zone Bridge"
          type: "Virtual"
          parent_interface: "bond0.070"
          mtu: "9000"

# VLAN 080 SDN VXLAN Zone

    - name: Netbox bond0.080 VLAN Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "bond0.080"
          description: "VXLAN Zone VLAN 802.1(p) 5"
          type: "Virtual"
          parent_interface: "bond0"
          mtu: "9000"
          mode: "Access"
          untagged_vlan:
            name: "VXLAN ZONE"

    - name: Netbox bond1.080 VLAN Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "bond1.080"
          description: "VXLAN Zone VLAN 802.1(p) 5"
          type: "Virtual"
          parent_interface: "bond1"
          mtu: "9000"
          mode: "Access"
          untagged_vlan:
            name: "VXLAN ZONE"

    - name: Netbox vmbr080 Bridge Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "vmbr080"
          description: "VXLAN Zone Bridge"
          type: "Virtual"
          parent_interface: "bond0.080"
          mtu: "9000"

    - name: Netbox VXLAN IP Address
      netbox.netbox.netbox_ip_address:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          assigned_object:
            device: "{{ my_hostname }}"
            name: "vmbr080"
          address: "10.80.8.2/16"
          dns_name: "vxlan.{{ my_hostname }}.cedar.mulhollon.com"

# VLAN 090 SDN EVPN Zone

    - name: Netbox bond0.090 VLAN Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "bond0.090"
          description: "EVPN Zone VLAN 802.1(p) 5"
          type: "Virtual"
          parent_interface: "bond0"
          mtu: "9000"
          mode: "Access"
          untagged_vlan:
            name: "EVPN ZONE"

    - name: Netbox bond1.090 VLAN Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "bond1.090"
          description: "EVPN Zone VLAN 802.1(p) 5"
          type: "Virtual"
          parent_interface: "bond1"
          mtu: "9000"
          mode: "Access"
          untagged_vlan:
            name: "EVPN ZONE"

    - name: Netbox vmbr090 Bridge Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "vmbr090"
          description: "EVPN Zone Bridge"
          type: "Virtual"
          parent_interface: "bond0.090"
          mtu: "9000"

    - name: Netbox EVPN IP Address
      netbox.netbox.netbox_ip_address:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          assigned_object:
            device: "{{ my_hostname }}"
            name: "vmbr090"
          address: "10.90.8.2/16"
          dns_name: "evpn.{{ my_hostname }}.cedar.mulhollon.com"

# VLAN 100 CEPH Public

    - name: Netbox bond0.100 VLAN Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "bond0.100"
          description: "CEPH Public VLAN 802.1(p) 6"
          type: "Virtual"
          parent_interface: "bond0"
          mtu: "9000"
          mode: "Access"
          untagged_vlan:
            name: "CEPH PUBLIC"

    - name: Netbox bond1.100 VLAN Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "bond1.100"
          description: "CEPH Public VLAN 802.1(p) 6"
          type: "Virtual"
          parent_interface: "bond1"
          mtu: "9000"
          mode: "Access"
          untagged_vlan:
            name: "CEPH PUBLIC"

    - name: Netbox vmbr100 Bridge Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "vmbr100"
          description: "CEPH Public Bridge"
          type: "Virtual"
          parent_interface: "bond1.100"
          mtu: "9000"

    - name: Netbox CEPH Public IP Address
      netbox.netbox.netbox_ip_address:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          assigned_object:
            device: "{{ my_hostname }}"
            name: "vmbr100"
          address: "10.100.8.2/16"
          dns_name: "cephpublic.{{ my_hostname }}.cedar.mulhollon.com"

# VLAN 110 CEPH Cluster

    - name: Netbox bond0.110 VLAN Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "bond0.110"
          description: "CEPH Cluster VLAN 802.1(p) 6"
          type: "Virtual"
          parent_interface: "bond0"
          mtu: "9000"
          mode: "Access"
          untagged_vlan:
            name: "CEPH CLUSTER"

    - name: Netbox bond1.110 VLAN Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "bond1.110"
          description: "CEPH CLUSTER VLAN 802.1(p) 6"
          type: "Virtual"
          parent_interface: "bond1"
          mtu: "9000"
          mode: "Access"
          untagged_vlan:
            name: "CEPH CLUSTER"

    - name: Netbox vmbr110 Bridge Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "vmbr110"
          description: "CEPH Cluster Bridge"
          type: "Virtual"
          parent_interface: "bond0.110"
          mtu: "9000"

    - name: Netbox CEPH Cluster IP Address
      netbox.netbox.netbox_ip_address:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          assigned_object:
            device: "{{ my_hostname }}"
            name: "vmbr110"
          address: "10.110.8.2/16"
          dns_name: "cephcluster.{{ my_hostname }}.cedar.mulhollon.com"

# VLAN 120 BACKUP

    - name: Netbox bond0.120 VLAN Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "bond0.120"
          description: "BACKUP VLAN 802.1(p) 1"
          type: "Virtual"
          parent_interface: "bond0"
          mtu: "9000"
          mode: "Access"
          untagged_vlan:
            name: "BACKUP"

    - name: Netbox bond1.120 VLAN Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "bond1.120"
          description: "BACKUP VLAN 802.1(p) 1"
          type: "Virtual"
          parent_interface: "bond1"
          mtu: "9000"
          mode: "Access"
          untagged_vlan:
            name: "BACKUP"

    - name: Netbox vmbr120 Bridge Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "vmbr120"
          description: "BACKUP Bridge"
          type: "Virtual"
          parent_interface: "bond0.120"
          mtu: "9000"

    - name: Netbox BACKUP IP Address
      netbox.netbox.netbox_ip_address:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          assigned_object:
            device: "{{ my_hostname }}"
            name: "vmbr120"
          address: "10.120.8.2/16"
          dns_name: "backup.{{ my_hostname }}.cedar.mulhollon.com"

# VLAN 200 TEST

    - name: Netbox bond0.200 VLAN Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "bond0.200"
          description: "TEST VLAN 802.1(p) 1"
          type: "Virtual"
          parent_interface: "bond0"
          mtu: "9000"
          mode: "Access"
          untagged_vlan:
            name: "TEST"

    - name: Netbox bond1.200 VLAN Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "bond1.200"
          description: "TEST VLAN 802.1(p) 1"
          type: "Virtual"
          parent_interface: "bond1"
          mtu: "9000"
          mode: "Access"
          untagged_vlan:
            name: "TEST"

    - name: Netbox vmbr200 Bridge Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "vmbr200"
          description: "TEST Bridge"
          type: "Virtual"
          parent_interface: "bond0.200"
          mtu: "9000"

# IPMI

    - name: Netbox IMPI Interface
      netbox.netbox.netbox_device_interface:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          device: "{{ my_hostname }}"
          name: "ipmi"
          description: "ipmi"
          type: "1000BASE-T (1GE)"
          speed: "1000000"
          duplex: "full"
          mtu: "1500"
          mode: "Access"
          mgmt_only: "true"
          untagged_vlan:
            name: "PROD"

    - name: Netbox IPMI IP Address
      netbox.netbox.netbox_ip_address:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          assigned_object:
            device: "{{ my_hostname }}"
            name: "ipmi"
          address: "10.10.6.52/16"
          dns_name: "ipmi.{{ my_hostname }}.cedar.mulhollon.com"

# Device Defaults

    - name: Netbox IPMI OOB Interface
      netbox.netbox.netbox_device:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          name: "{{ my_hostname }}"
          oob_ip: "10.10.6.52/16"

    - name: Netbox Primary IP Interface
      netbox.netbox.netbox_device:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        data:
          name: "{{ my_hostname }}"
          primary_ip4: "10.10.8.2/16"

#
