#!/usr/bin/env bash
#
# /usr/local/bin/krew_upgrade.sh
#
# Configured automatically by Ansible using file krew_upgrade.sh
#

kubectl krew update

# For a good time see this list of kubectl plugins
# https://github.com/ishantanu/awesome-kubectl-plugins

# context switching between multiple clusters
kubectl krew install ctx

# namespace switcher
kubectl krew install ns

# List outdated containers
kubectl krew install outdated

kubectl krew upgrade

exit 0
