#!/usr/bin/env bash
#
# /usr/local/bin/krew_install.sh
#
# Configured automatically by Ansible using file krew_install.sh
#

# A shameless cut and paste from:
# https://krew.sigs.k8s.io/docs/user-guide/setup/install/

# Don't forget to add ~/.krew/bin to your path in .bashrc or whatever

(
  set -x; cd "$(mktemp -d)" &&
  OS="$(uname | tr '[:upper:]' '[:lower:]')" &&
  ARCH="$(uname -m | sed -e 's/x86_64/amd64/' -e 's/\(arm\)\(64\)\?.*/\1\2/' -e 's/aarch64$/arm64/')" &&
  KREW="krew-${OS}_${ARCH}" &&
  curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/${KREW}.tar.gz" &&
  tar zxvf "${KREW}.tar.gz" &&
  ./"${KREW}" install krew
)

exit 0
