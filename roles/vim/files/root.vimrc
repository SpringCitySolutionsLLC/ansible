" Configured by Ansible using file root.vimrc
syntax off
set expandtab
set shiftwidth=4
set smarttab
set tabstop=4
colorscheme slate
set nocompatible
