#!/bin/bash
# ansible/upgrade_ansible.sh
# You might run this from ~/.local/bin but the authoritative copy is ansible/upgrade_ansible.sh

# Presumably you followed the instructions in README.md first

# So you can run this upgrade script directly
cp upgrade_ansible.sh ~/.local/bin

# Support modules
pip3 install --upgrade pip wheel

# Ansible
pip3 install --upgrade ansible
# Edit ~/.bash_aliases to include the lines
# export ANSIBLE_INVENTORY=/config/workspace/ansible/inventory
# echo Remember to source ~/workspace/ansible/.venv/bin/activate

# Ansible dev tools
# https://ansible.readthedocs.io/projects/lint/
pip3 install --upgrade ansible-dev-tools
pip3 install --upgrade ansible-lint

# Active Directory
# The docker container is not joined to the Active Directory
# This causes enormous headaches
#
# The solution at this point is using ssh to run samba-tool commands as root@dns01
#
# Interactive logins use SSO to log into root@dns01 (if they have access)
# The ansible docker container does NOT use kerberos
# So I need a way to get a kerberos TGT to use samba-tool
# At the TOP of the .bashrc file for root@dns01 add a line similar to
# echo -n "example" | kinit -V dedicated-samba-admin-acct@CEDAR.MULHOLLON.COM
# Before the [ -z "$PS1" ] && return line
# For interactive sessions this will re-obtain a ticket,
# and with my system design the only way to log into dns01 is
# if you already have a ticket; with the sole exception of a 
# direct SSH login from the ansible container, which will need 
# to obtain a ticket.
# For non-interactive sessions this will obtain a ticket, which 
# is great.
#
# TODO: There is a more elegant way to do this using keytabs
#
# TODO: Using root@dns01 is secure because anyone with access to root@dns01
# already has access to everything, however creating a dedicated jumpbox
# and dedicated local non-kerberos user might be a good idea.

# TODO AWS
# pip install --upgrade boto3
# AWS CLI no longer installed using pip see:
# https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html
# Edit ~/.bash_aliases to include the lines
# export AWS_ACCESS_KEY="example"
# export AWS_SECRET_KEY="example"
# export AWS_REGION="example"

# Kubernetes
pip3 install --upgrade kubernetes
# K8S access is something like this:
# mkdir ~/.kube
# scp root@rancher1:/root/.kube/config ./rancher.yaml
# scp root@rke1:/root/.kube/config ./rke.yaml
# scp vince@ubuntu:~/.kube/config ~/.kube/config
# Edit ~/.bash_aliases to include the lines
# export K8S_AUTH_KUBECONFIG=/config/.kube/config

# Netbox
pip3 install --upgrade netaddr pynetbox pytz packaging ansible-pylibssh
ansible-galaxy collection install -U netbox.netbox
# Edit ~/.bash_aliases to include the lines
# export NETBOX_TOKEN="example"
# export NETBOX_API="example"

# TODO Zabbix
pip3 install --upgrade zabbix-api
ansible-galaxy collection install -U ansible.posix community.general ansible.netcommon
ansible-galaxy collection install -U community.zabbix
# TODO document API key for Zabbix here

pip3 freeze > requirements.txt

echo
pip3 list --outdated

exit 0
