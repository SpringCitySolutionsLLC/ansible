README.md

# What is this?

These are PUBLIC ansible config files I use on my home LAN, that might be useful or inspirational or at least interesting for others to see.

This provides consistent configuration across all my systems, at extreme speed, for low effort.

# Installation

This is set up for use on the K8S "ansible-dev" deployment.

According to "python3 --version" both the codeserver and webtop run python 3.12.3

Initial setup on codeserver and webtop
~/scripts/initial.sh
cd ~/workspace/ansible
python3 -m venv .venv
source .venv/bin/activate
./upgrade_ansible.sh
On codeserver you'll want to open the workspace ansible.code-workspace and C-S-p Python:Create Environment...
Note you'll need a backup copy of ~/.ssh from the old ansible restored to /config/.ssh AND /root/.ssh
Also this assumes ~/.vault_password (not stored in this repo) contains the Ansible Vault password

Daily use on codeserver
open the workspace ./ansible.code-workspace
C-S-p Python:Create Environment... and use the probably existing .venv

Daily use on webtop
cd ~/workspaces/ansible
source .venv/bin/activate

Typical workflow:  See file gitflow.md in this repo

Have fun and play nice!

[![wakatime](https://wakatime.com/badge/user/df5e4008-6240-4426-9f05-40c18d2caa6b/project/f155a214-1c3f-40d0-b0ee-3269a7b199b4.svg)](https://wakatime.com/badge/user/df5e4008-6240-4426-9f05-40c18d2caa6b/project/f155a214-1c3f-40d0-b0ee-3269a7b199b4)