gitflow.md

# What is this?

My abbreviated notes on using Gitlab Flow in this project.
I used to store better notes in the Redmine project wiki but it seems convenient to place it with source code.
When this was a "private" project I used my "private" Redmine server.
Now that this is a "public" project I should use gitlab in public for everything, and move off the "private" Redmine.
Also when it was a "private" project I used the "private" Mattermost server, but now its "public" so I don't.

# References

Very high level overview
https://docs.gitlab.com/ee/topics/gitlab_flow.html

Git level overview
https://about.gitlab.com/topics/version-control/what-is-gitlab-flow/

Medium level discussion
https://about.gitlab.com/blog/2020/03/05/what-is-gitlab-flow/

Best Practices List
https://about.gitlab.com/topics/version-control/what-are-gitlab-flow-best-practices/

# Plan

For a bug, triage it.  Only Ansible level bugs should be in the Ansible project.
Verify, edit the subject line if necessary, document, replicate fault, and plan the fix.
The branch name will be something like "Bug-IssueNumber-ShortName"

For a feature, create a GitLab issue to design, plan, and track the new branch.
Assign to me, tag as "doing".
Notes should include paragraphs of What: Why: How: Measure of Success:
Pick a ShortName, the issue will be named "Feature ShortName" and the branch will be named "Feature-IssueNumber-ShortName" 

# Start Work

In the issue on Gitlab, Dropdown "Create Branch"
The default branch name is usually not very good.
Use: "Feature-IssueNumber-ShortName" or "Bug-IssueNumber-ShortName"
Button "Create Branch"

Add the branch locally
In VSC, "Source Control" "..." "Pull,Push" "Fetch (prune)" or
In the CLI on the webtop "git fetch --prune --verbose".

Checkout the new branch
In VS Code
Make sure your workspace is clean, no changes, pushed and pulled.
Select the new branch in the remote branches list. 
In Webtop if you want to go CLI mode
git pull origin master
git checkout -b "Feature-IssueNumber-ShortName"
git fetch
git branch
git branch --set-upstream-to=origin/Feature-IssueNumber-ShortName Feature-IssueNumber-ShortName
git pull
git status

# Work in the branch
BAU Business as usual.

# Complete work and do a merge
Move local back to master branch
git status (should be up to date)
git checkout master
git pull
git status (should be on master)

Create Merge Request online at Gitlab
On Gitlab in the issue, "New (Merge Request)"
Set Source branch to the new branch and Target branch to master.
Put useful verbiage in the description "Implements comments"
Assign to me
Usually check "Delete source branch when merge request is accepted"
Keep some old branches around for awhile, if there are many deletes in the branch that don't exist in the master branch.
Usually check "Squash commits when merge request is accepted"
"Create Merge Request"

Merge online at Gitlab
On Gitlab in the project, "Merge Requests", select the merge.
Click "Approve" then "Merge"

Pull the merged master
git pull
git status

Cleanup unused old branches
git branch
git branch -D "Feature-IssueNumber-ShortName"
git branch

# Release Notes
I'm not doing releases in this project, its ongoing permanently.

